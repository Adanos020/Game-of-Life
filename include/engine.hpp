#pragma once


// Program modules.
#include <simulation.hpp>

// SFML.
#include <SFML/Graphics.hpp>


#define FRAME_RATE 60
#define FRAME_TIME 1.0 / FRAME_RATE

struct Engine
{
private: // Fields.

        sf::RenderWindow window;
        Simulation       simulation;

public: // Methods.

        Engine(int wsize_x, int wsize_y);

        int run();

private: // Methods.

        void draw();
        void handle_input();
};