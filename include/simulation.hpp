#pragma once


// Program modules.
#include <matrix.hpp>

// SFML.
#include <SFML/Graphics.hpp>


struct Simulation
{
private: // Fields.

        bmatrix      grid;
        sf::Vector2i grid_size;

        bool paused;

        bool drawing;
        bool erasing;

        double generation_time;
        double lag;

        sf::RectangleShape cell;

public: // Methods.

        Simulation(size_t wsizex, size_t wsizey, size_t cellsiz, double gentime);
        ~Simulation();

        void randomize();
        void draw(sf::RenderTarget&);
        void update(double dt);
        void handle_input(sf::Event&);

private: // Methods.

        void handle_mouse(sf::Event&);
        void next_generation();
        int  living_neighbours(int row, int col);
        void destroy_grid();

        void kill_all();
        void set_all();
        void kill_vert();
        void set_vert();
        void kill_horiz();
        void set_horiz();
};