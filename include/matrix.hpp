#pragma once


// STDLIB.
#include <cstdio>


template<typename T>
T** new_matrix(size_t rows, size_t cols)
{
        T** mat = new T*[rows];
        for (size_t i = 0; i < rows; ++i)
        {
                mat[i] = new T[cols];
        }

        return mat;
}

typedef bool**  bmatrix;
typedef int**   imatrix;
typedef float** fmatrix;

inline bmatrix new_bmatrix(size_t rows, size_t cols)
{
        return new_matrix<bool>(rows, cols);
}

inline imatrix new_imatrix(size_t rows, size_t cols)
{
        return new_matrix<int>(rows, cols);
}

inline fmatrix new_fmatrix(size_t rows, size_t cols)
{
        return new_matrix<float>(rows, cols);
}