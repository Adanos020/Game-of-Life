# Project stuff.
cmake_minimum_required(VERSION 3.1)
project               (Game\ of\ Life)

# Build settings.
set(CMAKE_CXX_COMPILER clang++)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_FLAGS    "${CMAKE_CXX_FLAGS} -Wall -Wextra")

# Headers and source files.
include_directories(include)
file               (GLOB SOURCES src/*.cpp)

# Shared libraries.
set(PROJECT_LINK_LIBS libsfml-graphics.so
                      libsfml-window.so
                      libsfml-audio.so
                      libsfml-system.so)

# Build.
add_executable       (gol ${SOURCES})
target_link_libraries(gol ${PROJECT_LINK_LIBS})