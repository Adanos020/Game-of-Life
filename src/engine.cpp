// Program modules.
#include <engine.hpp>
#include <global.hpp>


Engine::Engine(int wsize_x, int wsize_y) :
        simulation(wsize_x, wsize_y, 15, 0.05)
{
        window.create(sf::VideoMode(wsize_x, wsize_y), "Game of Life", sf::Style::Close);
        window.setFramerateLimit(FRAME_RATE);
}

int Engine::run()
{
        sf::Clock clock;
        double lag = 0.0;

        while (window.isOpen())
        {
                handle_input();

                lag += clock.restart().asSeconds();
                while (lag >= FRAME_TIME)
                {
                        lag -= FRAME_TIME;
                        simulation.update(FRAME_TIME);
                }

                draw();
        }

        return 0;
}

void Engine::draw()
{
        window.clear();
        simulation.draw(window);
        window.display();
}

void Engine::handle_input()
{
        sf::Event event;
        while (window.pollEvent(event))
        {
                if (event.type == sf::Event::Closed)
                        window.close();

                if (event.type == sf::Event::KeyPressed &&
                    event.key.code == sf::Keyboard::Escape)
                        window.close();

                if (event.type == sf::Event::MouseMoved)
                        Global::mouse_position = sf::Mouse::getPosition(window);

                simulation.handle_input(event);
        }
}

int main()
{
        srand(time(0));

        Engine engine(1200, 900);
        return engine.run();
}