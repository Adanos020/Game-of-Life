// Program modules.
#include <global.hpp>
#include <simulation.hpp>

#include <cstdio>


#define whole_board (int i = 0; i < grid_size.x * grid_size.y; ++i)
#define all_neighbors (int x : {-1, 0, 1}) for (int y : {-1, 0, 1})


Simulation::Simulation(
        size_t wsizex, size_t wsizey,
        size_t cellsiz,
        double gentime
)
{
        // Generation time.
        generation_time = gentime;
        lag             = 0.0;
        paused          = true;
        drawing         = false;
        erasing         = false;

        // Cells.
        grid_size = sf::Vector2i(wsizex / cellsiz, wsizey / cellsiz);
        grid = new_bmatrix(grid_size.x, grid_size.y);
        randomize();

        // Graphical cell.
        cell.setSize(sf::Vector2f(cellsiz, cellsiz));
        cell.setOutlineColor(sf::Color::Black);
        cell.setOutlineThickness(1);
}

Simulation::~Simulation()
{
        destroy_grid();
}

void Simulation::randomize()
{
        for whole_board
        {
                grid[i % grid_size.x][i / grid_size.x] = rand() % 2;
        }
}

void Simulation::draw(sf::RenderTarget& target)
{
        int cellsiz = cell.getSize().x;
        for whole_board
        {
                cell.setPosition(cellsiz * (i % grid_size.x), cellsiz * (i / grid_size.x));
                cell.setFillColor(grid[i % grid_size.x][i / grid_size.x] ? sf::Color::Red : sf::Color::White);
                target.draw(cell);
        }
}

void Simulation::update(double dt)
{
        auto cell_pos = sf::Vector2i(Global::mouse_position.x / cell.getSize().x,
                                     Global::mouse_position.y / cell.getSize().y);

        if (    cell_pos.x >= 0 && cell_pos.x < grid_size.x &&
                cell_pos.y >= 0 && cell_pos.y < grid_size.y)
        {
                if (drawing && !erasing) grid[cell_pos.x][cell_pos.y] = true;
                if (!drawing && erasing) grid[cell_pos.x][cell_pos.y] = false;
        }
 
        if (paused) return;

        lag += dt;
        while (lag >= generation_time)
        {
                next_generation();
                lag -= generation_time;
        }
}

void Simulation::handle_input(sf::Event& event)
{
        handle_mouse(event);

        if (event.type == sf::Event::KeyPressed)
        {
                switch (event.key.code)
                {
                        case sf::Keyboard::Space:
                                paused = !paused;
                                break;

                        case sf::Keyboard::S:
                                next_generation();
                                break;

                        case sf::Keyboard::Down:
                                generation_time += 0.05;
                                break;

                        case sf::Keyboard::Up:
                                if (generation_time > 0.051)
                                        generation_time -= 0.05;
                                break;

                        case sf::Keyboard::C:
                                kill_all();
                                break;

                        case sf::Keyboard::B:
                                set_all();
                                break;

                        case sf::Keyboard::D:
                                set_horiz();
                                break;

                        case sf::Keyboard::F:
                                kill_horiz();
                                break;

                        case sf::Keyboard::G:
                                set_vert();
                                break;

                        case sf::Keyboard::H:
                                kill_vert();
                                break;

                        case sf::Keyboard::R:
                                randomize();
                                break;

                        default: break;
                }
        }
}

void Simulation::handle_mouse(sf::Event& event)
{
        if (event.type == sf::Event::MouseButtonPressed)
        {
                switch (event.mouseButton.button)
                {
                        case sf::Mouse::Left:
                                drawing = true;
                                break;

                        case sf::Mouse::Right:
                                erasing = true;
                                break;

                        default: break;
                }
        }
        else if (event.type == sf::Event::MouseButtonReleased)
        {
                switch (event.mouseButton.button)
                {
                        case sf::Mouse::Left:
                                drawing = false;
                                break;

                        case sf::Mouse::Right:
                                erasing = false;
                                break;

                        default: break;
                }
        }
}

void Simulation::next_generation()
{
        bmatrix new_grid = new_bmatrix(grid_size.x, grid_size.y);

        for whole_board
        {
                int x = i % grid_size.x;
                int y = i / grid_size.x;
                int neighbours = living_neighbours(x, y);

                /**/ if (!grid[x][y] && neighbours == 3)
                        new_grid[x][y] = true;
                else if (grid[x][y] && (neighbours < 2 || neighbours > 3))
                        new_grid[x][y] = false;
                else
                        new_grid[x][y] = grid[x][y];
        }

        destroy_grid();
        grid = new_grid;
}

int Simulation::living_neighbours(int row, int col)
{
        int neighbours = 0;

        for all_neighbors
        {
                int r = (row + x + grid_size.x) % grid_size.x;
                int c = (col + y + grid_size.y) % grid_size.y;

                neighbours += (int) grid[r][c];
        }
        neighbours -= (int) grid[row][col];

        return neighbours;
}

void Simulation::destroy_grid()
{
        for (int i = 0; i < grid_size.x; ++i)
        {
                delete[] grid[i];
        }
        delete[] grid;
}

void Simulation::kill_all()
{
        for whole_board
        {
                int x = i % grid_size.x;
                int y = i / grid_size.x;

                grid[x][y] = false;
        }
}

void Simulation::set_all()
{
        for whole_board
        {
                int x = i % grid_size.x;
                int y = i / grid_size.x;

                grid[x][y] = true;
        }
}

void Simulation::kill_vert()
{
        int y = Global::mouse_position.y / cell.getSize().y;
        for (int x = 0; x < grid_size.x; ++x)
        {
                grid[x][y] = true;
        }
}

void Simulation::kill_horiz()
{
        int x = Global::mouse_position.x / cell.getSize().x;
        for (int y = 0; y < grid_size.y; ++y)
        {
                grid[x][y] = false;
        }
}

void Simulation::set_vert()
{
        int y = Global::mouse_position.y / cell.getSize().y;
        for (int x = 0; x < grid_size.x; ++x)
        {
                grid[x][y] = true;
        }
}

void Simulation::set_horiz()
{
        int x = Global::mouse_position.x / cell.getSize().x;
        for (int y = 0; y < grid_size.y; ++y)
        {
                grid[x][y] = false;
        }
}